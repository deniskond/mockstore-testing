import { createSelector } from '@ngrx/store';

export const getCount = state => state.count;

export const getCountByAmount = (amount: number) => createSelector(
    getCount,
    count => count === amount ? count : 0,
);