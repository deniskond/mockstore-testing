import { ActionTypes, Increment, Decrement, Reset } from '../actions/data.actions';

export const initialState = 0;

export function counterReducer(state = initialState, action: Increment | Decrement | Reset): number {
    switch (action.type) {
        case ActionTypes.Increment:
            return state + action.payload.count;

        case ActionTypes.Decrement:
            return state - action.payload.count;

        case ActionTypes.Reset:
            return 0;

        default:
            return state;
    }
}
