import { Action } from '@ngrx/store';

export enum ActionTypes {
    Increment = '[Counter Component] Increment',
    Decrement = '[Counter Component] Decrement',
    Reset = '[Counter Component] Reset',
}

export class Increment implements Action {
    public readonly type = ActionTypes.Increment;

    constructor(readonly payload: { count: number }) {}
}

export class Decrement implements Action {
    public readonly type = ActionTypes.Decrement;

    constructor(readonly payload: { count: number }) {}
}

export class Reset implements Action {
    public readonly type = ActionTypes.Reset;
}
