import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Decrement, Increment, Reset } from './store/actions/data.actions';
import { take } from 'rxjs/operators';
import { getCount, getCountByAmount } from './store/selectors/data.selectors';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
    public count$: Observable<number>;
    public result: number;

    constructor(private store: Store<{ count: number }>) {}

    public ngOnInit(): void {
        this.count$ = this.store.pipe(select('count'));

        this.selectByParametrizedSelector(2);
    }

    public increment(count: number): void {
        this.store.dispatch(new Increment({ count }));
    }

    public decrement(count: number): void {
        this.store.dispatch(new Decrement({ count }));
    }

    public reset(): void {
        this.store.dispatch(new Reset());
    }

    public selectBySimpleSelector(): void {
        this.store
            .select(getCount)
            .pipe(take(1))
            .subscribe((count: number) => (this.result = count));
    }

    public selectByParametrizedSelector(amount: number): void {
        this.store
            .select(getCountByAmount(amount))
            .pipe(take(1))
            .subscribe((count: number) => (this.result = count));
    }

    public selectByPipeableSelector(amount: number): void {
        this.store
            .pipe(
                select(getCountByAmount(amount)),
                take(1),
            )
            .subscribe((count: number) => (this.result = count));
    }

    public addFive(): void {
        this.store
            .pipe(
                select('count'),
                take(1),
            )
            .subscribe((count: number) => (this.result = count + 5));
    }
}
